import json
import time
from datetime import datetime
import requests

print(datetime.now().strftime("%m/%d/%Y %H:%M:%S"))

hubspotdealurl = "https://api.hubapi.com/deals/v1/deal/paged?hapikey=4bf4f35c-4a59-49d7-92ae-bdbc4355f8a5&includeAssociations=true&limit=250&properties=dealname&offset="
hubspotdeals = []

payload={}
headers = {}

response = requests.request("GET", hubspotdealurl + "0", headers=headers, data=payload)
responsedata = json.loads(response.text)

if "status" in responsedata:
  print("hubspot api request failed")
  # log_file.write('\nhubspot api request failed\n')
else:
  hubspotdeals += responsedata["deals"]

  while responsedata["hasMore"]:
    payload = {}
    headers = {}
    response = requests.request("GET", hubspotdealurl + str(responsedata["offset"]), headers=headers, data=payload)
    responsedata = json.loads(response.text)
    hubspotdeals += responsedata["deals"]

  # with open('hubspotdeals.json', 'w') as outfile:
  #   json.dump(hubspotdeals, outfile)


businessaccounts = {}

url = "https://bodiometerhomerestapi-dot-project-4839952831808961167.appspot.com"

payload={}
headers = {
  'password': '746C419785D84DC8B3A20B9195363105'
}

response = requests.request("POST", url + "/api/token/dbo", headers=headers, data=payload)

if response.text == "Invalid Credentials":
  print("Invalid Credentials")
  log_file.write('\nInvalid Credentials\n')
else:
  accesscode = response.text

  payload={}
  headers = {
    'Authorization': 'Bearer ' + accesscode
  }

  response = requests.request("GET", url + "/api/businessaccounts/v1/all/details/full", headers=headers, data=payload)

  pyresponse = json.loads(response.text)

  if not pyresponse["success"]:
    print("database error")
    # log_file.write('\ndatabase error\n')
  else:
    businessaccounts = pyresponse["data"]["businessAccounts"]
    # with open('businessaccounts.json', 'w') as outfile:
    #   json.dump(businessaccounts, outfile)

# with open('hubspotdeals.json') as json_file:
#   hubspotdeals = json.load(json_file)
#
# with open('businessaccounts.json') as json_file:
#   businessaccounts = json.load(json_file)


for deal in hubspotdeals:
  accid = deal["properties"]["dealname"]["value"].split("-")[-1].strip()
  if accid in businessaccounts:
    expirydate = ""
    expirydatetimestamp = ""
    for license in businessaccounts[accid]["businessLicenses"]:
      dt = datetime.strptime(businessaccounts[accid]["businessLicenses"][license]["expiryDate"], "%m/%d/%Y %H:%M:%S")
      dt_now = datetime(year=dt.year, month=dt.month, day=dt.day, hour=0, minute=0, second=0, microsecond=0)
      gmtime = time.gmtime(dt_now.timestamp())
      expirydatetimestamp = int(dt_now.timestamp() - gmtime.tm_hour * 3600) * 1000

    url = "https://api.hubapi.com/deals/v1/deal/" + str(deal["dealId"]) + "?hapikey=4bf4f35c-4a59-49d7-92ae-bdbc4355f8a5"

    payload = json.dumps({
      "properties": [
        {
          "name": "license_expiry_date",
          "value": expirydatetimestamp
        }
      ]
    })
    headers = {
      'Content-Type': 'application/json'
    }

    response = requests.request("PUT", url, headers=headers, data=payload)

    print(accid, " successful")

    # log_file.write("\n" + str(accid) + "\n")
    # log_file.write(response.text)
    # log_file.write("\n\n")
